module.exports = function (grunt) {

	grunt.initConfig({
		clean: {
			dist: 'dist'
		},
		sass: {
			options: {
				sourceMap: true,
				outputStyle: "nested",
				imagePath: "/images"

			},
			dist: {
				files: {
					'dist/main.min.css': 'assets/stylesheets/main.scss'
				}
			}
		},
		copy: {
		  main: {
			expand: true,
			cwd: 'assets/images',
			src: ['**', '!*.psd', '!for-favicons.png'],
			dest: 'dist/images'
		  }
		},
		
		 watch: {

		 	css: {
		 		files: ['**/*.scss'],
		 		tasks: ['sass'],
		 		options: {
		 			spawn: false
		 		}
		 	}
		 }

	});


	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask('default', 'sass');
	grunt.registerTask('dist', ['clean', 'copy', 'sass']);

};